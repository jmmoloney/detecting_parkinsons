import pandas as pd
import os

###############################################################################
#                           Feature Engineering 1                             #
###############################################################################

"""
@author: Jessica
"""

data = pd.read_csv("preprocessed_data/prefeature_user_data.csv")
og_files = os.listdir("data/Tappy Data") 
print("Creating basic summary statistic features...")
#################################################
#                Starting Year                  #
#################################################
"""
Determines the patient's starting year based on the earliest "Tappy" file in the dataset

"""
start_year = {}

for filename in og_files:   #of the form userid_YYMM.txt
    details = filename.split("_")
    id = details[0]
    year = details[1][:2]
    # records earliest start year per id in a dictionary
    if id in start_year:
        if start_year[id] > year:
            start_year[id] = year
    else:
        start_year[id] = year
  
data["start_year"] = data["User_ID"].map(start_year).fillna(-1).astype("int64")

#################################################
#                Starting Age                   #
#################################################
"""
Determines the patient's age at the start of the observational period for that patient
based on their starting year and birth year.

    - If birthyear not available, fill with "-1" (possibly impute at a later date)
"""

def fill_age(row):
    if (row["BirthYear"] == -1) or (row["start_year"] == -1):
        return -1
    else:
        return row["start_year"]+2000 - row["BirthYear"]
    
data["age"] = data.apply(fill_age, axis = 1)


#################################################
#              Number of Entries               #
#################################################
"""
Done in the preprocessing stage as `count`

"""
###############################################################################
#                         Summary Variables per User                          #
###############################################################################
"""
@author: Kyle

Creates variables that summarize information taken from the "Tappy" keystroke data.
    - Variables include, mean hold time, mean flight time etc., as well as their 
      standard deviations
"""

# variables to create summaries for
variables = ['Hold_Time', 'Latency_Time', 'Flight_Time']
summary_names = ['User_ID', 'HT_mu', 'HT_std', 'LT_mu', 'LT_std', 'FT_mu', 'FT_std']
summary = []

indices = pd.Index(data['User_ID'])
folders_list = 'preprocessed_data/tappy_data/'

for folder in os.listdir(folders_list):
    if folder == 'rest':
        continue
    for filename in os.listdir(folders_list + folder):
        file = pd.read_csv(os.path.join(folders_list + folder, filename))

        user_id = filename[:-4]
        row = [user_id]
        
        for var in variables:
            row.append(file[var].mean())
            row.append(file[var].std())
        
        summary.append(row)

summary = pd.DataFrame(summary, columns=summary_names)
summary = summary.fillna(0)
out = data.merge(summary, on = "User_ID")
out.to_csv('preprocessed_data/selected_user_data.csv', index = False)   