#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 29 20:49:59 2018

@author: Kyle
"""

import pandas as pd
import os

###############################################################################
#                           Feature Engineering 2                             #
###############################################################################

selected_user_data = pd.read_csv('preprocessed_data/selected_user_data.csv')

folders_list = 'preprocessed_data/tappy_data/'
print("Creating additional statistical features...")
#################################################
#           Summary Variables per User          #
#################################################

summary = []
for folder in os.listdir(folders_list):
    if folder == 'rest':
        continue
    for filename in os.listdir(folders_list + folder):
        file = pd.read_csv(os.path.join(folders_list + folder, filename))

        user_id = filename[:-4]
        row = [user_id]
        
        hold_med = file['Hold_Time'].median()
        lat_med = file['Latency_Time'].median()
        
        L_data = file.loc[file['Hand'] == 'L']['Hold_Time']
        R_data = file.loc[file['Hand'] == 'R']['Hold_Time']
        
        L_avg = L_data.mean()
        L_med = L_data.median()
        L_std = L_data.std()
        L_skew = L_data.skew()
        L_kurt = L_data.kurtosis()
        
        R_avg = R_data.mean()
        R_med = R_data.median()
        R_std = R_data.std()
        R_skew = R_data.skew()
        R_kurt = R_data.kurtosis()
        
        L_R_diff = L_avg - R_avg
        
        RL_data = file.loc[file['Direction'] == 'RL']['Latency_Time']
        LR_data = file.loc[file['Direction'] == 'LR']['Latency_Time']
        
        RL_avg = RL_data.mean()
        RL_med = RL_data.median()
        RL_std = RL_data.std()
        RL_skew = RL_data.skew()
        RL_kurt = RL_data.kurtosis()
        
        LR_avg = LR_data.mean()
        LR_med = LR_data.median()
        LR_std = LR_data.std()
        LR_skew = LR_data.skew()
        LR_kurt = LR_data.kurtosis()
        
        RL_LR_diff = RL_avg - LR_avg
        
        values = [hold_med, lat_med, L_avg, L_med, L_std, L_skew, L_kurt, R_avg, R_med, R_std, R_skew, R_kurt, L_R_diff, \
                  RL_avg, RL_med, RL_std, RL_skew, RL_kurt, LR_avg, LR_med, LR_std, LR_skew, LR_kurt]
        for value in values:
            row.append(value)
        
        summary.append(row)

new_info = pd.DataFrame(summary, columns = ['User_ID', 'Hold_med', 'Lat_med', 'L_avg', 'L_med', 'L_std', 'L_skew', \
                                            'L_kurt', 'R_avg', 'R_med', 'R_std', 'R_skew', \
                                            'R_kurt', 'L_R_diff', 'RL_avg', 'RL_med', 'RL_std', 'RL_skew', 'RL_kurt', \
                                            'LR_avg', 'LR_med', 'LR_std', 'LR_skew', 'LR_kurt'])
new_info = new_info.fillna(0)
out = selected_user_data.merge(new_info, on = "User_ID")
#numeric_columns = out.select_dtypes(include=['int64','float64'])
#for col in out.columns:
#    if col in ['In_Train','In_Test','In_Hold']:
#        continue
#    if col in numeric_columns:
#        out[col] = out[col].replace('-1', out[col].mean())
#        out[col] = (out[col]/out[col].max())
out.to_csv('preprocessed_data/selected_user_features.csv', index = False)
        