import matplotlib.pyplot as plt
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import VotingClassifier, RandomForestClassifier, BaggingClassifier
from sklearn.metrics import f1_score, accuracy_score,precision_score,recall_score, classification_report
import pandas as pd
from sklearn.svm import SVC, NuSVC
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn import pipeline
from sklearn.externals import joblib
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis

##### LOADING AND SETTING UP DATA
parkData = pd.read_csv('preprocessed_data/selected_user_features.csv', header = 0)
parkData['Parkinsons'] = parkData['Parkinsons'].astype(int)
testData = parkData[parkData['In_Test'] == 1]
trainData = parkData[(parkData['In_Train'] == 1 )| (parkData['In_Hold'] == 1)]

h_feats = ['HT_std', 'Hold_med', 'L_med', 'L_std', 'L_skew', \
            'L_kurt', 'R_med', 'R_std', 'R_skew', \
            'R_kurt', 'L_R_diff']
l_feats = ['LT_std', 'Lat_med', 'RL_med', 'RL_std', 'RL_skew', 'RL_kurt', \
            'LR_med', 'LR_std', 'LR_skew', 'LR_kurt']

train, test = train_test_split(trainData, train_size = 0.8)

X_h = trainData[h_feats]
X_l = trainData[l_feats]
y = trainData['Parkinsons']

X_h_hold = test[h_feats]
X_l_hold = test[l_feats]
y_hold = test['Parkinsons']

X_h_test = testData[h_feats]
X_l_test = testData[l_feats]
y_test = testData['Parkinsons']

indiv_score = []
####### Creating dumb function
lda_h = LinearDiscriminantAnalysis().fit(X_h, y)
lda_l = LinearDiscriminantAnalysis().fit(X_l, y)

def ht(data):
    return lda_h.transform(data)

def lt(data):
    return lda_l.transform(data)

X_h = ht(X_h)
X_l = lt(X_l)
    
############################## MODELS #########################################
lda = LinearDiscriminantAnalysis()

###### Support Vector Machine
print("Fitting SVC models...")
svm = SVC(probability = True)

steps = [('svm', svm)]

parameters = dict(svm__C=[1.0, 0.2], svm__kernel= ["poly","sigmoid"], #linear’,‘precomputed’
                  svm__degree=[2,3,4,5],
                  svm__gamma=['auto'],
                  svm__shrinking=[True], svm__tol=[0.001], svm__max_iter=[-1], svm__random_state=[733])

pipe = pipeline.Pipeline(steps)

svm_m = GridSearchCV(pipe, parameters, cv = 10)
svm_m = BaggingClassifier(svm_m)
svm_h = svm_m.fit(X_h, y)
svm_l = svm_m.fit(X_l, y)
indiv_score.append("SVM: Hold {} {}, Test {} {}".format(f1_score(y_hold, svm_h.predict(ht(X_h_hold))), 
                   f1_score(y_hold, svm_l.predict(lt(X_l_hold))), f1_score(y_test, svm_h.predict(ht(X_h_test))),
                   f1_score(y_test, svm_l.predict(lt(X_l_test)))))

###### Multi Level Perceptron
print("Fitting MLP models...")
mlp = MLPClassifier()

steps = [('lda', lda),
         ('mlp', mlp)]

parameters = dict(mlp__hidden_layer_sizes = [(100,30,10),(58,5,3)], 
                  mlp__activation = ["relu", "tanh"],
                  mlp__solver = ["adam", "lbfgs"], 
                  mlp__warm_start = [True], 
                  mlp__max_iter = [2000])

pipe = pipeline.Pipeline(steps)

mlp_m = GridSearchCV(pipe, parameters, cv = 10)
mlp_m = BaggingClassifier(mlp_m)
mlp_h = mlp_m.fit(X_h, y)
mlp_l = mlp_m.fit(X_l, y)
indiv_score.append("MLP: Hold {} {}, Test {} {}".format(f1_score(y_hold, mlp_h.predict(ht(X_h_hold))), 
                   f1_score(y_hold, mlp_l.predict(lt(X_l_hold))), f1_score(y_test, mlp_h.predict(ht(X_h_test))),
                   f1_score(y_test, mlp_l.predict(lt(X_l_test)))))

###### Logistic Regression
print("Fitting LR models...")
lr = LogisticRegression()

steps = [('lda', lda),
         ('lr', lr)]

parameters = dict(lr__penalty = ['l1', 'l2'], 
              lr__tol = [0.0001, 0.00001, 0.001], 
              lr__max_iter = [10, 100, 1000])

pipe = pipeline.Pipeline(steps)

lr_m = GridSearchCV(pipe, parameters, cv = 10)
lr_m = BaggingClassifier(lr_m)
lr_h = lr_m.fit(X_h, y)
lr_l = lr_m.fit(X_l, y)
indiv_score.append("LR: Hold {} {}, Test {} {}".format(f1_score(y_hold, lr_h.predict(ht(X_h_hold))), 
                   f1_score(y_hold, lr_l.predict(lt(X_l_hold))), f1_score(y_test, lr_h.predict(ht(X_h_test))),
                   f1_score(y_test, lr_l.predict(lt(X_l_test)))))

###### Random Forest

print("Fitting RF models...")
rf = RandomForestClassifier()

steps = [('lda', lda),
         ('rf', rf)]

parameters = dict(rf__n_estimators = [30,50], 
                  rf__max_features = [None, 0.5, 0.2], 
                  rf__min_samples_split = [2, 4, 8],
                  rf__max_depth = [5, 10, 3, 15],
                  rf__oob_score = [True],
                  rf__min_impurity_decrease = [0.001, 0.01])

pipe = pipeline.Pipeline(steps)

rf_m = GridSearchCV(pipe, parameters, cv = 10)
rf_m = BaggingClassifier(rf_m)
rf_h = rf_m.fit(X_h, y)
rf_l = rf_m.fit(X_l, y)
indiv_score.append("RF: Hold {} {}, Test {} {}".format(f1_score(y_hold, rf_h.predict(ht(X_h_hold))), 
                   f1_score(y_hold, rf_l.predict(lt(X_l_hold))), f1_score(y_test, rf_h.predict(ht(X_h_test))),
                   f1_score(y_test, rf_l.predict(lt(X_l_test)))))

###### Nu-Support Classification
print("Fitting NSVC models...")
nusvc = NuSVC(probability = True)

steps = [('lda', lda),
         ('nusvc', nusvc)]

parameters = dict(nusvc__nu = [0.4, 0.5, 0.6],
                  nusvc__kernel = ['linear', 'rbf'])

pipe = pipeline.Pipeline(steps)

nusvc_m = GridSearchCV(pipe, parameters, cv = 10)
nusvc_m = BaggingClassifier(nusvc_m)
nusvc_h = nusvc.fit(X_h, y)
nusvc_l = nusvc.fit(X_l, y)
indiv_score.append("NSVC: Hold {} {}, Test {} {}".format(f1_score(y_hold, nusvc_h.predict(ht(X_h_hold))), 
                   f1_score(y_hold, nusvc_l.predict(lt(X_l_hold))), f1_score(y_test, nusvc_h.predict(ht(X_h_test))),
                   f1_score(y_test, nusvc_l.predict(lt(X_l_test)))))

###### Decision Tree
print("Fitting DT models...")
dt = DecisionTreeClassifier()

steps = [('lda', lda),
         ('dt', dt)]

parameters = dict(dt__max_depth = [None],
                  dt__min_samples_split = [2, 4, 6],
                  dt__min_samples_leaf = [1, 2, 3],
                  dt__max_features = [None, 'sqrt'])

pipe = pipeline.Pipeline(steps)

dt_m = GridSearchCV(pipe, parameters, cv = 10)
dt_m = BaggingClassifier(dt_m)
dt_h = dt_m.fit(X_h, y)
dt_l = dt_m.fit(X_l, y)
indiv_score.append("DT: Hold {} {}, Test {} {}".format(f1_score(y_hold, dt_h.predict(ht(X_h_hold))), 
                   f1_score(y_hold, dt_l.predict(lt(X_l_hold))), f1_score(y_test, dt_h.predict(ht(X_h_test))),
                   f1_score(y_test, dt_l.predict(lt(X_l_test)))))

###### KNN
print("Fitting KNN models...")
knn = KNeighborsClassifier()

steps = [('lda', lda),
         ('knn', knn)]

parameters = dict(
                  knn__n_neighbors = [2,3,4,5,6,7,8],
                  knn__weights = ['uniform', 'distance'])

pipe = pipeline.Pipeline(steps)

knn_m = GridSearchCV(pipe, parameters, cv = 10)
knn_m = BaggingClassifier(knn_m)
knn_h = knn_m.fit(X_h, y)
knn_l = knn_m.fit(X_l, y)
indiv_score.append("KNN: Hold {} {}, Test {} {}".format(f1_score(y_hold, knn_h.predict(ht(X_h_hold))), 
                   f1_score(y_hold, knn_l.predict(lt(X_l_hold))), f1_score(y_test, knn_h.predict(ht(X_h_test))),
                   f1_score(y_test, knn_l.predict(lt(X_l_test)))))

###### QDA
print("Fitting QDA models...")
qda = QuadraticDiscriminantAnalysis()

steps = [('lda', lda),
         ('qda', qda)]

parameters = dict(qda__reg_param=[0.0, 1, 0.2], qda__tol=[0.0001])

pipe = pipeline.Pipeline(steps)

qda_m = GridSearchCV(pipe, parameters, cv = 10)
qda_m = BaggingClassifier(qda_m)
qda_h = qda_m.fit(X_h, y)
qda_l = qda_m.fit(X_l, y)
indiv_score.append("QDA: Hold {} {}, Test {} {}".format(f1_score(y_hold, qda_h.predict(ht(X_h_hold))), 
                   f1_score(y_hold, qda_l.predict(lt(X_l_hold))), f1_score(y_test, qda_h.predict(ht(X_h_test))),
                   f1_score(y_test, qda_l.predict(lt(X_l_test)))))

############## SAVING MODELS FOR LATER USE
models = [svm_h, svm_l, mlp_h, mlp_l, lr_h, lr_l, rf_h, rf_l, nusvc_h, nusvc_l,
          dt_h, dt_l, knn_h, knn_l, qda_h, qda_l]
model_names = ['svm_h', 'svm_l', 'mlp_h', 'mlp_l', 'lr_h', 'lr_l', 'rf_h', 'rf_l', 'nusvc_h', 'nusvc_l',
          'dt_h', 'dt_l', 'knn_h', 'knn_l', 'qda_h', 'qda_l']
for i in range(len(models)):
    joblib.dump(models[i], 'models/b_{}.pkl'.format(str(model_names[i])))
    

joblib.dump(lda_h, 'models/lda_h.pkl')
joblib.dump(lda_l, 'models/lda_l.pkl')


