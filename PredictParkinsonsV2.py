import matplotlib.pyplot as plt
import mpld3
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import VotingClassifier, RandomForestClassifier, BaggingClassifier
from sklearn.metrics import f1_score, accuracy_score,precision_score,recall_score, classification_report
import pandas as pd
from sklearn.svm import SVC, NuSVC
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn import pipeline
from sklearn.externals import joblib
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis

###### Reading in the Features used for modelling
parkData = pd.read_csv('preprocessed_data/selected_user_features.csv', header = 0)
parkData['Parkinsons'] = parkData['Parkinsons'].astype(int)
testData = parkData[parkData['In_Test'] == 1]
trainData = parkData[(parkData['In_Train'] == 1 )| (parkData['In_Hold'] == 1)]

#### Splitting feautres into two groups and thne into 
h_feats = ['HT_std', 'Hold_med', 'L_med', 'L_std', 'L_skew', \
            'L_kurt', 'R_med', 'R_std', 'R_skew', \
            'R_kurt', 'L_R_diff']
l_feats = ['LT_std', 'Lat_med', 'RL_med', 'RL_std', 'RL_skew', 'RL_kurt', \
            'LR_med', 'LR_std', 'LR_skew', 'LR_kurt']

X_h = trainData[h_feats]
X_l = trainData[l_feats]
y = trainData['Parkinsons']

X_h_test = testData[h_feats]
X_l_test = testData[l_feats]
y_test = testData['Parkinsons']

###### Loading in the models fitted in ModelPrep.py
print("Loading in previously trained models...")
svm_h, svm_l = joblib.load('models/b_svm_h.pkl'), joblib.load('models/b_svm_l.pkl')
mlp_h, mlp_l = joblib.load('models/b_mlp_h.pkl'), joblib.load('models/b_mlp_l.pkl')
lr_h, lr_l = joblib.load('models/b_lr_h.pkl'), joblib.load('models/b_lr_l.pkl')
rf_h, rf_l = joblib.load('models/b_rf_h.pkl'), joblib.load('models/b_rf_l.pkl')
nusvc_h, nusvc_l = joblib.load('models/b_nusvc_h.pkl'), joblib.load('models/b_nusvc_l.pkl')
dt_h, dt_l = joblib.load('models/b_dt_h.pkl'), joblib.load('models/b_dt_l.pkl')
knn_h, knn_l = joblib.load('models/b_knn_h.pkl'), joblib.load('models/b_knn_l.pkl')
qda_h, qda_l = joblib.load('models/b_qda_h.pkl'), joblib.load('models/b_qda_l.pkl')
lda_h, lda_l = joblib.load('models/lda_h.pkl'), joblib.load('models/lda_l.pkl')
models = [svm_h, svm_l, mlp_h, mlp_l, lr_h, lr_l, rf_h, rf_l, nusvc_h, nusvc_l,
          dt_h, dt_l, knn_h, knn_l, qda_h, qda_l, lda_h, lda_l]
model_names = ['svm_h', 'svm_l', 'mlp_h', 'mlp_l', 'lr_h', 'lr_l', 'rf_h', 'rf_l', 'nusvc_h', 'nusvc_l',
          'dt_h', 'dt_l', 'knn_h', 'knn_l', 'qda_h', 'qda_l', 'lda_h', 'lda_l']

for i in range(len(models)):
    models[i] = joblib.load('models/{}.pkl'.format(model_names[i]))
    
def ht(data):
    return lda_h.transform(data)

def lt(data):
    return lda_l.transform(data)

###### Converting data with LDA transformation
X_h = ht(X_h_test)
X_l = lt(X_l_test)

####### MEAN PROBABILITY CLASSIFIER
#"""
print("Predicting using Mean Probability Classifier...")
model_outputs = []
for i in range(len(models[:-2])):
    if model_names[i][-1] == 'h':
        model_outputs.append(models[i].predict_proba(X_h)[:,1])
    else:
        model_outputs.append(models[i].predict_proba(X_l)[:,1])

predictions = []
for i in range(len(model_outputs[0])):
    h_sum = 0
    l_sum = 0
    for j in range(len(model_outputs)):
        if j % 2 == 0:
            h_sum += model_outputs[j][i]
        else:
            l_sum += 0.5 + 0.7*(model_outputs[j][i] - 0.5)
    out = (h_sum + l_sum)/(len(model_outputs))
    predictions.append(int(round(out)))

f1 = f1_score(y_test, predictions)
acc = accuracy_score(y_test, predictions)
prec = precision_score(y_test, predictions)
rec = recall_score(y_test, predictions)
print("Output scores are (f1, accuracy, precision, recall)...")
print("({},{},{},{})".format(f1, acc, prec, rec))

#"""
####### VOTING CLASSIFIER
"""
model_outputs = []
for i in range(len(models[:-2])):
    if model_names[i][-1] == 'h':
        model_outputs.append(models[i].predict(X_h))
    else:
        model_outputs.append(models[i].predict(X_l))

predictions = []
for i in range(len(model_outputs[0])):
    pred_sum = 0
    for j in range(len(model_outputs)):
        pred_sum += model_outputs[j][i]
    if pred_sum >= 5:
        predictions.append(1)
    else:
        predictions.append(0)

"""
####### RECORDING RESULT DATA FOR INDIVIDUAL MODELS
"""
reg_scores = []
bag_scores = []
for i in range(len(models)):
    print('At model {}...'.format(model_names[i]))
    
    if model_names[i][-2:] == '_h':
        models[i].fit(X_h, y)
        predictions = models[i].predict(ht(X_h_test))
        bag_model = BaggingClassifier(models[i]).fit(X_h, y)
        bag_predictions = bag_model.predict(ht(X_h_test))
    else:
        models[i].fit(X_l, y)
        predictions = models[i].predict(lt(X_l_test))
        bag_model = BaggingClassifier(models[i]).fit(X_l, y)
        bag_predictions = bag_model.predict(lt(X_l_test))
    
    f1 = f1_score(y_test, predictions)
    acc = accuracy_score(y_test, predictions)
    prec = precision_score(y_test, predictions)
    rec = recall_score(y_test, predictions)
    
    record = [model_names[i]]
    record.append((f1, acc, prec, rec))
    reg_scores.append(record)
    

    f1 = f1_score(y_test, bag_predictions)
    acc = accuracy_score(y_test, bag_predictions)
    prec = precision_score(y_test, bag_predictions)
    rec = recall_score(y_test, bag_predictions)
    
    record = [model_names[i]]
    record.append((f1, acc, prec, rec))
    
    bag_scores.append(record)
"""