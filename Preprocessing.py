import pandas as pd
import os
import shutil
from collections import defaultdict

print("Beginning Preprocessing stage...")
###############################################################################
#                    Preprocessing "Archived users" Data                      #
###############################################################################

"""
@author: Kyle

Moving user data previously stored in separate text files into one csv file

"""
print("Creating user_data csv...")
user_data = []

for file in os.listdir('data/Archived users'):
    with open(os.path.join('data/Archived users', file)) as txt_file:       
        row = {}
        id = file[5:-4]
        row["User_ID"] = id
        
        for line in txt_file:
            label, data = line.split(': ')
            
            # If entry is empty or filled with dashes
            if len(data) < 2 or '-' in data:
                row[label] = '-1'
                continue
            
            row[label] = data[:-1]
            
        user_data.append(row)

user_data = pd.DataFrame(user_data)

###############################################################################
#                        Preprocessing "Tappy" Data                           #
###############################################################################

"""
@author: Jessica

Appending multiple files attributed to one user into one text file

This is done by:
    
   - Creating dictionary entries of the form {user_id : [file1.txt, file2.txt, ...]} where
     the key is the user id and the values are a list of text files attributed to that user id.

   - Cycling through each user and copying all contents of their files into one master file
 
"""

print("Joining together user logs...")
files = os.listdir("data/Tappy Data")

# Creating dictionary with default action to append values when duplicate key arises
ids = defaultdict(list)

for filename in files:
    id = filename.split("_")[0]
    ids[id].append(filename)      

directory = "preprocessed_data/tappy_data/"
if not os.path.exists(directory):
    os.makedirs(directory)
    
for key in ids:
    with open(directory + key + ".txt", 'wb') as master:
        for file in ids[key]:
            with open("data/Tappy Data/{}".format(file),'rb') as f:
                shutil.copyfileobj(f, master)
      
          
###############################################################################
#                    Preprocessing "Tappy Data" Entries                       #
###############################################################################

"""
@author: Kyle

Removing lines with corrupted entries by first detecting when entries 
into should-be numeric columns are not numeric and then dropping the observation

""" 
print("Fixing messed up data... Takes a while!")
column_names = ['User_ID', 'Date', 'Timestamp', 'Hand', 'Hold_Time', 'Direction', \
                'Latency_Time', 'Flight_Time']

fixed_dir = 'preprocessed_data/fixed_tappy_data/'
if not os.path.exists(fixed_dir):
    os.makedirs(fixed_dir)
count = {}
files = os.listdir('preprocessed_data/tappy_data')
tracker = len(files)

for file in files:
    if not file.startswith("."): #no hidden files
        
        if tracker % 5 == 0:
            print("{} files left to fix.".format(tracker))
        tracker = tracker - 1   
        
        # reading fixed width files
        fixed = pd.read_fwf('preprocessed_data/tappy_data/{}'.format(file), names = column_names)
        
        #numeric columns
        cols = ['Date', 'Hold_Time', 'Latency_Time', 'Flight_Time']
        
        done = False
        offset = 0
        while done is False:
            try:
                fixed[cols] = fixed[cols].apply(pd.to_numeric, errors='coerce', axis=1)
                done = True
            except Exception as e:
                err_mes = str(e)
                print(err_mes.split('index ')[1].split('\'')[0])
                what_index = int(err_mes.split('index ')[1].split('\'')[0])
                fixed = fixed.drop(fixed.index[what_index - offset])
                offset = offset + 1
                
                
        for col in cols:
            fixed = fixed[fixed[col].notnull()]
        id = file.split(".")[0]   
        count[id] =  len(fixed)
        # fixing files and replacing in place
        fixed.to_csv('preprocessed_data/fixed_tappy_data/{}'.format(file), index = False)

for file in files:
    os.remove(os.path.join('preprocessed_data/tappy_data', file))

fixed_dir = 'preprocessed_data/fixed_tappy_data'
for file in os.listdir(fixed_dir):
    os.rename(os.path.join(fixed_dir, file), os.path.join('preprocessed_data/tappy_data', file))

os.rmdir('preprocessed_data/fixed_tappy_data')
###############################################################################
#                          Number of Entries Per User                         #
###############################################################################  
"""
@author: Jessica 

Counting the number of keystrokes per user and affixing it to the "User Data" csv file.
This will assist in selecting only those above an acceptable threshold of entries.

"""       
print("Calculating number of entries per user...")   
user_data["count"] = user_data["User_ID"].map(count).fillna(0).astype("int64")
user_data.to_csv('preprocessed_data/user_data.csv', index = False)
print("Finished Preprocessing!")

