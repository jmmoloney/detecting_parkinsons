# Detecting Parkinson's Disease from Typing Behaviour

## How to Run
In order to run the project through all stages, run 'RunProject.py'.  

## Project Summary
The project prepared covers the topic of predicting Parkinson’s Disease using data collected from everyday typing activities. 
This topic and dataset were chosen based on an Australian study with the same research problem. After preprocessing, 
summary statistics of the keylog files are used as features for the machine learning pipeline. Multiple individual models 
were then trained using bagging to reduce variability, as well as cross-validation, using the Scikit-learn library. 
Those results were fed into an ensemble model that aggregated predictions. An F-score of 0.83 and a recall of 1.00 were 
achieved on our best model.  

### The Data
The data is currently hosted on Kaggle at the following link: https://www.kaggle.com/valkling/tappy-keystroke-data-with-parkinsons-patients  
The data and project idea come from the following article titled "High-accuracy detecting of early Parkinson's Disease using multiple characteristics of
finger movement while typing", written by Warwick R. Adams. The article can be found here: http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0188226#sec008
The raw data and preprocessed data used for this project may also be found in the 'data' and the 'preprocessed_data' folders in the repo.

### Exploration
Guided Exploratory Data Analysis may be found in the jupyter notebook titled 'Exploratory_DataAnalysis.ipynb'.  
Contingency tables, visualizations and comments are included.  

### Findings
The findings can be further explored throughout the report (titled 'report.pdf') and created youtube video found in the `media` folder.  
Our best model achieved an F-score of 0.83 and a recall of 1.00.  

### Tools
* Python
* Scikit-Learn
* Pandas
* Matplotlib/ Seaborn
* Spark
* MLib
* D3
* Mpld3

### Challenges and What to Look For
* Small sample sizes/ High amount of variance in the data/models
* Implementing Bootstrapping and Bagging to help above challenge
* Ensemble models (Majority voting classifier/ Mean Porbability Classifier)
* Understanding what methods might work well and when to try non-linear methods


### Notes
Extra created visualizations may be found in the `visualization` folder. 
Code for D3 visualizations may be found in the jupyter notebook and python files located in the `visualization`.