import os
import pandas as pd

###############################################################################
#                             Test Set Creation                               #
############################################################################### 

"""
@author: Jessica
"""
users = pd.read_csv("preprocessed_data/user_data.csv")
files = os.listdir("preprocessed_data/tappy_data")
print("Creating training/testing sets...")

############################################
#     Set Proportions and Thresholds       #
############################################ 
"""
Creates a training set, test set and optionally a holdout set if the
training fraction and the testing fraction do not add to 1.
"""

threshold = 2000
training_frac = 0.7
test_frac = 0.2
seed = 1000

############################################
#             Set Creation                 #
############################################
"""
Selects from patients who either do not have Parkinson's Disease or have "Mild" severity,
as well as those who have "enough" keystroke data as specified by a threshold
"""

data = users.loc[(users["Impact"].isin(["Mild", "-1"])) \
                    & (users["count"] >= threshold)]

training = data.sample(frac = training_frac, random_state = seed, replace = False)
rest = data.loc[~data.index.isin(training.index)]

test = rest.sample(frac = test_frac/(1 - training_frac), random_state = seed + 1, replace = False)
hold = rest.loc[~rest.index.isin(test.index)]

training.loc[:, 'In_Train'] = 1
training.loc[:, 'In_Test'] = 0
training.loc[:, 'In_Hold'] = 0

test.loc[:, 'In_Train'] = 0
test.loc[:, 'In_Test'] = 1
test.loc[:, 'In_Hold'] = 0

hold.loc[:, 'In_Train'] = 0
hold.loc[:, 'In_Test'] = 0
hold.loc[:, 'In_Hold'] = 1

training_ids = list(training['User_ID'])
test_ids = list(test['User_ID'])
hold_ids = list(hold['User_ID'])

############################################
#             Folder Creation              #
############################################
print("Separating files by training/testing groups...")
os.chdir('preprocessed_data/tappy_data')

selected_data_folders= ['training', 'test', 'hold', 'rest']
for folder in selected_data_folders:
    if not os.path.exists(folder):
        os.makedirs(folder)

for file in training_ids:
    file_name = file + '.txt'
    if os.path.exists(file_name):
        os.rename(file_name, os.path.join('training', file_name))

for file in test_ids:
    file_name = file + '.txt'
    if os.path.exists(file_name):
        os.rename(file_name, os.path.join('test', file_name))
        
for file in hold_ids:
    file_name = file + '.txt'
    if os.path.exists(file_name):
        os.rename(file_name, os.path.join('hold', file_name))

for file in os.listdir():
    if file not in selected_data_folders:
        os.rename(file, os.path.join('rest', file))
    
os.chdir('..')
os.chdir('..')

print("Creating selected_user_data.csv...")
selected_users = pd.concat([training, test, hold])
selected_users.to_csv('preprocessed_data/prefeature_user_data.csv', index = False)


