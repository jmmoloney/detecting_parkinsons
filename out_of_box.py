import matplotlib.pyplot as plt
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import f1_score, accuracy_score,precision_score,recall_score 
from pyspark.sql import SparkSession, types
from pyspark.sql import functions as f
from pyspark.ml import Pipeline
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.tuning import CrossValidator, ParamGridBuilder
from pyspark.ml.evaluation import (BinaryClassificationEvaluator, 
                                MulticlassClassificationEvaluator)
from pyspark.ml.classification import (LogisticRegression, 
                                        LinearSVC,
                                        RandomForestClassifier, 
                                        GBTClassifier,
                                        MultilayerPerceptronClassifier)
import pandas as pd
import os

###############################################################################
#                         Training and Prediction                             #
############################################################################### 
"""
Main file for training, tuning and model selection in order to produce a final model
"""

spark = SparkSession.builder.appName('parkinsons_model').getOrCreate()

pd_df = pd.read_csv('preprocessed_data/selected_user_data.csv', header = 0)
pd_df['Parkinsons'] = pd_df['Parkinsons'].astype(int)


############################################
#            Spark Dataframes              #
############################################
parkData = spark.createDataFrame(pd_df)
trainData = parkData.where(parkData['In_Train'] == 1)
holdData = parkData.where(parkData['In_Hold'] == 1)
testData = parkData.where(parkData['In_Test'] == 1)

############################################
#                 Features                 #
############################################
features = ['HT_mu','HT_std','LT_mu','LT_std','FT_mu', 'FT_std']
featuresAssembler = VectorAssembler(inputCols= features, outputCol='features')

############################################
#                Evaluators                #
############################################

def evaluate(result):
    scores = {}
    predictionAndLabels = result.select("prediction", "Parkinsons")
    metrics = ["f1", "weightedPrecision","weightedRecall","accuracy"]
    for m in metrics:
        evaluator = MulticlassClassificationEvaluator(labelCol='Parkinsons', metricName=m)
        scores[m] = str(evaluator.evaluate(predictionAndLabels))
        #scores.append(str(m) + ": " + str(evaluator.evaluate(predictionAndLabels)))
    return scores

evaluator = MulticlassClassificationEvaluator(labelCol='Parkinsons') #f1 by default

def py_evaluate(truth, prediction):
    scores = {}
    metrics = [("f1", f1_score), ("weightedPrecision", precision_score), 
               ("weightedRecall", recall_score), ("accuracy",  accuracy_score)]
    
    for name, m in metrics:
        scores[name] = (m(truth, prediction))
    return scores

############################################
#             Score Recorder               #
############################################
def record_scores(score, baseline = False):
    est = score[0]
    val_dict = {"features": str(features)}
    
    for metric, metric_val in score[2].items():
        #metric, metric_val = value.split(': ')
        val_dict[metric] = metric_val
    if not baseline:
        for param_name, param_value in score[3].items():
            #param_name, param_value = param.split(':  ')
            val_dict[param_name] = param_value
        
    df_dict = {0: val_dict}
    filename = '{}.csv'.format(str(est).split('_')[0])
    if baseline:
        filename = 'baseline.csv'
        val_dict["model"] = str(est).split('_')[0]
    try:
        old_records = pd.read_csv("model_results/" + filename, header = 0)
        new_record = pd.DataFrame.from_dict(df_dict, orient = 'index')
        updated_records = pd.concat([old_records, new_record], ignore_index = True)
        updated_records.to_csv("model_results/" + filename, columns = val_dict.keys(), index = False)
    except FileNotFoundError:
        records = pd.DataFrame.from_dict(df_dict, orient = 'index')
        records.to_csv("model_results/" + filename, columns = val_dict.keys(), index = False)
    
    return   
###############################################################################
#                          Out-of-Box Modelling                               #
###############################################################################
"""
Training and Testing "Out-of-box" models. "Out-of-box" refers to models trained
on the training set with default parameters and "basic" features.
Testing is done on a "hold-out" set which accounts for about 10% of the total 
dataset being worked on. This is done to avoid overfitting the train and test
sets and as a result, underestimating the generalization error.
"""

############################################
#                   Models                 #
############################################ 

# Spark #
logisticModel = LogisticRegression(labelCol='Parkinsons')
rfModel = RandomForestClassifier(labelCol='Parkinsons', seed = 39)
gbtModel = GBTClassifier(labelCol='Parkinsons', seed = 142)
mlpModel = MultilayerPerceptronClassifier(labelCol='Parkinsons', layers = [len(features),20,2], seed = 123)
svmModel = LinearSVC(labelCol='Parkinsons')

# Python #
knnModel = KNeighborsClassifier(n_neighbors = 5)
############################################
#                Training                 #
############################################
train = trainData.cache()
test = holdData.cache()

models = [
    ('Logistic Regression', Pipeline(stages=[featuresAssembler, logisticModel])),
    ('Random Forest', Pipeline(stages=[featuresAssembler, rfModel])),
    ('Gradient Boosted Trees', Pipeline(stages=[featuresAssembler, gbtModel])),
    ('MultiLayer Perceptron', Pipeline(stages=[featuresAssembler, mlpModel])),
    ('Support Vector Machine', Pipeline(stages=[featuresAssembler, svmModel]))
            ]

for label, pipeline in models:
    
    model = pipeline.fit(train)
    predictions = model.transform(test)
    train_pred = model.transform(train)
    train_scores = evaluate(train_pred)
    
    
    print('\n')
    print('Scores for model {}...'.format(label))
    print('############################################## \nTraining Score:')
   
    for name, label_score in train_scores.items():
        print(str(name) + ": " + str(label_score))
        
    test_scores = evaluate(predictions)
    print('\nTest Score:')
        
    for name, label_score in test_scores.items():
        print(str(name) + ": " + str(label_score))
        
    score = [model.stages[-1], train_scores, test_scores]
    record_scores(score, baseline = True)
    print('##############################################')
          
py_models = [("KNearestNeighbours", knnModel)]      

pd_train = train.toPandas()
X = pd_train[features]
y = pd_train["Parkinsons"]
pd_test = test.toPandas()
X_test = pd_test[features]
y_test = pd_test["Parkinsons"]

for label, pymodel in py_models:
    model = pymodel.fit(X,y)
    train_pred = model.predict(X)
    train_scores = py_evaluate(y, train_pred)
    test_pred = model.predict(X_test)
    test_scores = py_evaluate(y_test, test_pred)
    
    print('\n')
    print('Scores for model {}...'.format(label))
    print('############################################## \nTraining Score:')
   
    for name, label_score in train_scores.items():
        print(str(name) + ": " + str(label_score))
        
    print('\nTest Score:')
        
    for name, label_score in test_scores.items():
        print(str(name) + ": " + str(label_score))
        
    score = [label, train_scores, test_scores]
    record_scores(score, baseline = True)
    print('##############################################')
    
###############################################################################
#                           k-fold Cross Validation                           #
###############################################################################
"""
Tuning hyperparameters in order to improve performance on "out of bag" models.
We use 5-fold cross validation on our training set and test on our hold out set.

The code used below is adapted from Greg Baker's CMPT732 "assignment 5b".

"""
folds = 5

# TODO: deal with the param_list being outside the function
param_list = []

def estimator_gridbuilder(estimator, paramnames_values):
    pgb = ParamGridBuilder()
    for pn, vals in paramnames_values.items():
        assert hasattr(vals, '__iter__'), "List of values required for each parameter name"
        pgb.addGrid(estimator.getParam(pn), vals)
        if pn not in param_list:
            param_list.append(pn)
    return estimator, pgb
        
estimator_gridbuilders = [
    estimator_gridbuilder(
        LogisticRegression(),
        dict(regParam=[0, 0.1, 0.01, 0.001],
             maxIter=[100, 10])),
    
    estimator_gridbuilder(
            GBTClassifier(),
            dict(maxIter = [30, 50],
                 maxDepth = [10, 5],
                 seed = [732])),
            
    estimator_gridbuilder(
            RandomForestClassifier(),
            dict(numTrees = [50],
                 maxDepth = [10, 5],
                 seed = [40]))    ]
   
column_names = dict(featuresCol="features",
                        labelCol="Parkinsons",
                        predictionCol="prediction")

cv_list = []   
for est, pgb in estimator_gridbuilders:
        est = est.setParams(**column_names)
        
        pl = Pipeline(stages=[featuresAssembler, est])

        paramGrid = pgb.build()
        cv_list.append((est, CrossValidator(estimator=pl,
                                             estimatorParamMaps=paramGrid,
                                             evaluator= evaluator,
                                             numFolds = folds)))        

print("\nPreparing Cross Validation Stage...")   
left = len(cv_list) 
for est, cv in cv_list:
    print("{} Models left to check...".format(left))
    left = left - 1
    model = cv.fit(train)
    train_pred = model.transform(train)
    train_scores = evaluate(train_pred)
    test_pred = model.transform(test)
    test_scores = evaluate(test_pred)
    score = [est, train_scores, test_scores]
    param_set = {}
    for param in param_list:
        if model.bestModel.stages[1]._java_obj.hasParam(param):
            param_set[param] = str(model.bestModel.stages[1]._java_obj.extractParamMap().\
                               apply(model.bestModel.stages[1]._java_obj.getParam(param)))
    score.append(param_set)
    record_scores(score)
    #print(score)
###############################################################################
#                                k-NN Tuning                                  #
###############################################################################    

k_list = list(range(1,11))
k_train = {}
k_test = {}

def best_knn(k = 5):
    knnModel = KNeighborsClassifier(n_neighbors = k).fit(X,y)
    train_pred = knnModel.predict(X)
    train_scores = py_evaluate(y, train_pred)
    k_train[k] = train_scores
    test_pred = knnModel.predict(X_test)
    test_scores = py_evaluate(y_test, test_pred)
    k_test[k] =  test_scores
    return

for i in k_list:
    best_knn(i)

metrics = ['f1', 'precision', 'recall', 'accuracy']

#def knn_plot(dict, knnmodel)
ktrain_df = pd.DataFrame.from_dict(k_train, orient = "index")
ktest_df = pd.DataFrame.from_dict(k_test, orient = "index")

ktest_df['features'] = str(features)
try:
    knn_records = pd.read_csv('model_results/knn.csv', header = 0)
    out = pd.concat([knn_records, ktest_df], ignore_index = True)
    out.to_csv('model_results/knn.csv', index = False)
except FileNotFoundError:
    ktest_df.to_csv('model_results/knn.csv', index = False)


cmap = ["navy", "blue", "dodgerblue", "cyan"]
c = 0
for metric in ktrain_df.columns:
    plt.plot(k_list, ktrain_df[metric],marker = 's', label = metric, markersize=5, c = cmap[c])
    c += 1
plt.title("Optimizing K-Nearest Neighbours (Training Set)")
plt.xticks(k_list)
plt.ylabel('Score')
plt.xlabel("k")
plt.legend()
plt.savefig("knn_train.png")
print('saving knn plot to local directory ...')
plt.close()

c = 0
for metric in ktest_df.columns:
    plt.plot(k_list, ktest_df[metric],marker = 's', label = metric, markersize=5, c = cmap[c])
    c += 1
plt.title("Optimizing K-Nearest Neighbours (Hold-Out Set)")
plt.ylabel('Score')
plt.xlabel("k")
plt.xticks(k_list)
plt.legend()
plt.savefig("knn_hold.png")
print('saving knn plot to local directory ...')
plt.close()

os.system('say "your program has finished"')    
