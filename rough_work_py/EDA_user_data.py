import pandas as pd

data = pd.read_csv("preprocessed_data/user_data.csv")

data.groupby(by = ["Parkinsons","Gender"], as_index= False)["User_ID"].count()

"""
   Parkinsons  Gender  User_ID
0       False  Female       25
1       False    Male       33
2        True  Female       79
3        True    Male       90  
"""
data.groupby(by = ["Parkinsons"], as_index= False)["User_ID"].count()

"""
   Parkinsons  User_ID
0       False       58
1        True      169
"""

data.groupby(by = ["Parkinsons","Levadopa"], as_index= False)["User_ID"].count()

"""
   Parkinsons  Levadopa  User_ID
0       False     False       58
1        True     False       57
2        True      True      112
"""

data.groupby(by = ["Parkinsons","Impact"], as_index= False)["User_ID"].count()

"""
   Parkinsons  Impact  User_ID
0       False      -1       55
1       False    Mild        3
2        True      -1        4
3        True  Medium       74
4        True    Mild       67
5        True  Severe       24
"""

data.groupby(by = ["Parkinsons","Tremors"], as_index= False)["User_ID"].count()

"""
   Parkinsons  Tremors  User_ID
0       False    False       55
1       False     True        3
2        True    False       68
3        True     True      101
"""
data.groupby(by = ["Parkinsons","UPDRS"], as_index= False)["User_ID"].count()

"""
   Parkinsons       UPDRS  User_ID
0       False  Don't know       58
1        True           1        1
2        True           2        2
3        True           3        3
4        True           4        2
5        True  Don't know      161

"""