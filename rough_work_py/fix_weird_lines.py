import pandas as pd
import os

column_names = ['User Key', 'Date', 'Timestamp', 'Hand', 'Hold Time', 'Direction', \
                'Latency Time', 'Flight Time']

for file in os.listdir('preprocessed_data/tappy_data'):

    fixed = pd.read_fwf('preprocessed_data/tappy_data/{}'.format(file), names = column_names)

    cols = ['Date', 'Hold Time', 'Latency Time', 'Flight Time']
    
    done = False
    count = 0
    while done is False:
        try:
            fixed[cols] = fixed[cols].apply(pd.to_numeric, errors='coerce', axis=1)
            done = True
        except Exception as e:
            err_mes = str(e)
            print(err_mes.split('index ')[1].split('\'')[0])
            what_index = int(err_mes.split('index ')[1].split('\'')[0])
            fixed = fixed.drop(fixed.index[what_index - count])
            count = count + 1
            
            
    for col in cols:
        fixed = fixed[fixed[col].notnull()]
    
    fixed.to_csv('preprocessed_data/Fixed/{}'.format(file), index = False)