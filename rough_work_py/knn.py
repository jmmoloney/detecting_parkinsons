"""
@author: Jessica

"""
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import f1_score, accuracy_score,precision_score,recall_score 
import pandas as pd

pd_df = pd.read_csv('preprocessed_data/selected_user_data.csv', header = 0)
pd_df['Parkinsons'] = pd_df['Parkinsons'].astype(int)

train = pd_df.loc[pd_df["In_Train"] == 1]
test = pd_df.loc[pd_df["In_Hold"] == 1]

features = ['HT_mu','HT_std','LT_mu','LT_std','FT_mu', 'FT_std']
X = train[features]
target = train["Parkinsons"]


X_test = test[features]
test_target = test["Parkinsons"]


neigh = KNeighborsClassifier(n_neighbors = 3)
model = neigh.fit(X, target)
train_pred = model.predict(X)
test_pred = model.predict(X_test)


def py_evaluate(truth, prediction):
    scores = []
    metrics = [f1_score, accuracy_score, precision_score, recall_score]
    for m in metrics:
        scores.append(str(m) + ": " + str(m(truth, prediction)))
    return scores
train_scores = py_evaluate(target, train_pred)
test_scores = py_evaluate(test_target, test_pred)

print(train_scores)
print(test_scores)