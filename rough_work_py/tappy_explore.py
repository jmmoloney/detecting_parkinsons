import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt

input_names = ['User Key', 'Date', 'Timestamp', 'Hand', 'Hold Time', 'Direction', \
                'Latency Time', 'Flight Time']
summary_names = ['User_Key', 'Parkinson', 'HT_mu', 'HT_std', 'LT_mu', 'LT_std', 'FT_mu', 'FT_std']

summary = []

user_data = pd.read_csv('preprocessed_data/user_data.csv')
user_data = user_data[['User_ID', 'Parkinsons']]
user_data['Parkinsons'] = user_data['Parkinsons'].astype(int)

list_user_data = list(user_data['User_ID'])
list_tappy_user = []

for file in os.listdir('preprocessed_data/tappy_data/Fixed'):
    user_id = file[:-4]
    list_tappy_user.append(user_id)

missing_user_data = [id for id in list_tappy_user if id not in list_user_data] 

indices = pd.Index(user_data['User_ID'])
for file in os.listdir('preprocessed_data/tappy_data/Fixed'):
    
    data_file = pd.read_csv(os.path.join('preprocessed_data/tappy_data/Fixed', file))

    user_id = file[:-4]
    if user_id in missing_user_data:
        continue
    if user_id == 'PRGWGP1WH4':
        inspect = data_file
    
    summary_line = [user_id]
    parkinsons = user_data.iloc[indices.get_loc(user_id)]["Parkinsons"]
    summary_line.append(parkinsons)
    
    variables = ['Hold Time', 'Latency Time', 'Flight Time']
    for var in variables:
        summary_line.append(data_file[var].mean())
        summary_line.append(data_file[var].std())
    
    summary.append(summary_line)

summary = pd.DataFrame(summary, columns=summary_names)
summary = summary.fillna(0)
summary.to_csv('preprocessed_data/fixed_data_summary.csv', index = False)   