"""
@author: Jessica
"""

import os
import shutil
from collections import defaultdict


files = os.listdir("data/Tappy Data")

ids = defaultdict(list)

for file in files:
    id = file.split("_")[0]
    ids[id].append(file)      

directory = "preprocessed_data/tappy_data/"
if not os.path.exists(directory):
    os.makedirs(directory)
    
for key in ids:
    with open(directory + key + ".txt", 'wb') as wfd:
        for f in ids[key]:
            with open("data/Tappy Data/{}".format(f),'rb') as fd:
                shutil.copyfileobj(fd,wfd)