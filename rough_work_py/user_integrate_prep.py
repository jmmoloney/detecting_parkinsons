import pandas as pd
import os

columnNames = ['User_ID', 'Birth_Year', 'Gender', 'Parkinsons', 'Tremors', \
               'Diag_Year', 'Sided', 'UPDRS', 'Impact', 'Levadopa', 'DA', 'MOAB', \
               'Other']
user_data = []

for file in os.listdir('Data/Archived users'):
    with open(os.path.join('Data/Archived users', file)) as txt_file:
        
        new = []
        new.append(file[5:-4])
        
        for line in txt_file:
            
            label, data = line.split(': ')
            
            if len(data) < 2 or '-' in data:
                new.append('-1')
                continue
            
            new.append(data[:-1])
            
        user_data.append(new)

user_data = pd.DataFrame(user_data)
user_data.columns = columnNames    
user_data.to_csv('user_data.csv', index = False)